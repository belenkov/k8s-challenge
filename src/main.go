package main

import (
	"errors"
	"io"
	"flag"
	"fmt"
	"net/http"
	"os"
)

func getRoot(w http.ResponseWriter, r *http.Request) {
	io.WriteString(w, "Hello janbo from Dmitry\n")
}

func main() {
	http.HandleFunc("/", getRoot)

        port := flag.String("port","8080","app port")
	flag.Parse()
	err := http.ListenAndServe(":" + *port, nil)
        if errors.Is(err, http.ErrServerClosed) {
		fmt.Printf("server closed\n")
	} else if err != nil {
		fmt.Printf("error starting server: %s\n", err)
		os.Exit(1)
	}
}
