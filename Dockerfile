FROM golang:1.19.4 as builder

WORKDIR /build-dir

COPY src .

RUN go build


FROM busybox:glibc

USER nobody

COPY --chown=nobody:nobody --from=builder /build-dir/simple-server /

ENTRYPOINT ["/simple-server"]
